package server.db;

import server.exception.WorkerException;

import java.sql.*;
import java.util.Objects;

public class DBConnector {

    private Connection dbcon;
    private final String[] statements = {
            "Select pwtries from User Where ID = ?",
            "Select * From User Where ID = ? and PWHASH = ?",
            "Update User Set pwtries = ? Where ID = ?",
            "Select Name, Ort, Zeit From User Join Zeitstempel on UserID = ID Where ID = ? Order by Zeit",
            "Select Name, Ort, Zeit From User Join Zeitstempel" +
                    " on UserID = User.ID Where User.ID = ? and Zeit between ? and ? Order by Zeit",
            "Update User Set PWHASH = ? Where ID = ?",
            "Select pintries from User Where ID = ?",
            "Select * From User Where ID = ? and PinHash = ?",
            "Update User Set pintries = ? Where ID = ?",
            "Update User Set PinHash = ? Where ID = ?",
            "Select GruppenID, Name, Zeit From User left outer join Zeitstempel " +
                    "on ID = UserID and substr(Zeit,0,11) = ? " +
                    "where GruppenID = ? " +
                    "Group by Name",
            "Select * From Reader Where Key = ?",
            "Select UserID From Chips Where Chipnummer = ?",
            "Select Timeloggable from Gruppen join User" +
                    " on Gruppen.Name = GruppenID Where User.ID = ?",
            "Select Ort From Reader Where ID = ? and Key = ?",
            "Insert into Zeitstempel (UserID, Ort, Zeit) Values (?, ?, ?)",
            "Select Lagerberechtigt from Gruppen join User on Gruppen.Name = GruppenID Where User.ID = ?",
            "Insert into Lagerlog (UserID, Zeit) Values (?, ?)",
            "Select Gruppen.Name from Gruppen join User" +
                    " on Gruppen.Name = GruppenID Where User.ID = ?",
            "Select Obergruppe from Gruppenstruktur Where Untergruppe = ?",
            "Select Untergruppe from Gruppenstruktur Where Obergruppe = ?"
    };

    public void connect(String path) throws SQLException {
        dbcon = DriverManager.getConnection("jdbc:sqlite:"+path);
    }

    public void executeUpdate(SQLStatements statement, String[] parameters) throws SQLException {
        PreparedStatement pstmt = dbcon.prepareStatement(statements[statement.ordinal()]);
        for (int i=1;i<=parameters.length;i++) {
            pstmt.setString(i,parameters[i-1]);
        }
        pstmt.executeUpdate();
    }

    public ResultSet executeQuery(SQLStatements statement, String[] parameters, WorkerException e) throws WorkerException, SQLException {
        PreparedStatement pstmt = dbcon.prepareStatement(statements[statement.ordinal()]);
        for (int i=1;i<=parameters.length;i++) {
            pstmt.setString(i,parameters[i-1]);
        }
        ResultSet tmp = pstmt.executeQuery();
        if (Objects.requireNonNull(tmp).isClosed() && e != null) {
            throw e;
        }
        return tmp;
    }

}
