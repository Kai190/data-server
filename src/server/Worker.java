package server;

import server.db.DBConnector;
import server.db.SQLStatements;
import hash.Hasher;
import server.exception.*;
import server.log.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class Worker extends Thread{

    private static DBConnector dbcon;
    static void setDbcon(DBConnector d) {
        dbcon = d;
    }

    private byte version;
    private Socket con;
    private InputStream in;
    private OutputStream out;
    private KeyPair kp;
    private PublicKey pkclient;
    private Logger logger;
    private InetAddress client;
    private final byte nul = 0;                                                                             //nullterminator and status ok

    Worker(Socket c, KeyPair k, Logger l) throws IOException {
        logger = l;                                                                                         //init values from server.Server
        version = 1;
        con = c;
        client = con.getInetAddress();
        try {
            in = c.getInputStream();
            out = con.getOutputStream();
        } catch (IOException e) {
            logger.logFormatted(" Streams not initialized. Connection closed", client.toString());
            cleanup();
            throw e;
        }
        kp = k;
    }

    @Override
    public void run() {
        try {
            KeyFactory kf = KeyFactory.getInstance("RSA");
            RSAPublicKeySpec spec = kf.getKeySpec(kp.getPublic(),RSAPublicKeySpec.class);                       //generate Modulus and Exponent from publickey
            out.write(version);                                                                                 //server.Server-hello
            out.write(spec.getModulus().toString().getBytes(StandardCharsets.UTF_8));
            out.write(nul);
            out.write(spec.getPublicExponent().toString().getBytes(StandardCharsets.UTF_8));
            out.write(nul);

            try {
                String user = decrypt(Base64.getDecoder().decode(readToNull()));                                //Header-client
                String pw = Hasher.hash(decrypt(Base64.getDecoder().decode(readToNull())));

                if (user.equals("reader")) {                                                                    //reader
                    reader(pw);
                } else {
                    user(user, pw, kf);
                }
            } catch (SQLException e) {
                logger.logFormatted("SQLStatement failed :" + e.getMessage(), client.toString());
                e.printStackTrace();
                out.write((byte) 1);
                out.flush();
            } catch (WorkerException e) {
                logger.logFormatted(e.getMessage(), client.toString());
                out.write(e.getNum());
                out.flush();
            } catch (BadPaddingException | IllegalBlockSizeException | InvalidKeyException | NoSuchPaddingException e) {
                logger.logFormatted("Reading Error: " + e.getMessage(), client.toString());
                out.write((byte) 1);
                out.flush();
            }

        } catch (IOException e) {
            logger.logFormatted("In- Out- or ConnectionError: " + e.getMessage(), client.toString());

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            logger.logFormatted("Conversion of/to Key failed", client.toString());

        }
        cleanup();
    }

    private void user(String user, String pw, KeyFactory kf) throws WorkerException, SQLException, IOException, InvalidKeySpecException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        ResultSet tmp;
        int pwtries;
        tmp = dbcon.executeQuery(SQLStatements.GETPWTRIES,
                new String[]{user},
                new UserNotFoundUser("User " + user + " not found"));
        pwtries = (tmp.getInt(1));
        if(pwtries >=3) {
            throw new TooManyPWFails("To many failed Tries. user/pwhash: " + user + "/" + pw);
        }

        try {
            dbcon.executeQuery(SQLStatements.CHECKPW, new String[]{user, pw}, new LoginFailed("Login failed. user/pwhash: " + user + "/" + pw)); //check user identity
        } catch (Exception e) {//Increment tries
            dbcon.executeUpdate(SQLStatements.UPDATEPWTRIES, new String[]{
                    String.valueOf(++pwtries), user});
            throw e;
        }

        dbcon.executeUpdate(SQLStatements.UPDATEPWTRIES, new String[]{"0", user});
        RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(readToNull()), new BigInteger(readToNull()));     //Header-client Part 2
        pkclient = kf.generatePublic(spec);
        out.write(nul);                                                                        //Header-answer
        out.flush();
        logger.logFormatted("User: " + user + " logged in and Key received", client.toString());

        int c = in.read();
        if(c==-1) {
            out.write((byte) 1);
        }
        String target = decrypt(Base64.getDecoder().decode(readToNull()));                        //user-send
        boolean higher = isHigherAuthentificated(user, target, c);
        if(target.equals(user)||higher) {
            logger.logFormatted("User: " + user + " is permissioned", client.toString());
            switch (c) {                                                                           //modes
                case 0:                                                                               //0 all data
                    tmp = dbcon.executeQuery(SQLStatements.GETTIMES, new String[]{target}, null);
                    out.write(nul);
                    out.flush();

                    sendResult(setToString(tmp, 3));
                    logger.logFormatted("Send Result: " + target, client.toString());
                    break;

                case 1:                                                                               //1 date-intervall
                    String bdate = decrypt(Base64.getDecoder().decode(readToNull()));
                    String edate = decrypt(Base64.getDecoder().decode(readToNull()));
                    tmp = dbcon.executeQuery(SQLStatements.GETTIMESBYDATE, new String[]{target, bdate, edate}, null);     //user-send Part 2

                    out.write(nul);
                    out.flush();
                    sendResult(setToString(tmp, 3));
                    logger.logFormatted("Send Result: " + target, client.toString());
                    break;

                case 2:                                                                               //2 PWUpdate
                    String newpw = Hasher.hash(decrypt(Base64.getDecoder().decode(readToNull())));
                    dbcon.executeUpdate(SQLStatements.SETPW, new String[]{newpw, target});                       //user-send Part 2
                    logger.logFormatted("Changed PW: " + target, client.toString());
                    out.write(nul);
                    out.flush();
                    break;

                case 3:                                                                               //3 PINUpdate
                    if(!higher){
                        String oldpin = Hasher.hash(decrypt(Base64.getDecoder().decode(readToNull())));
                        int pintries;
                        tmp = dbcon.executeQuery(SQLStatements.GETPINTRIES,
                                new String[]{target}, new UserNotFoundUser("User: " + user + " not found"));
                        pintries = tmp.getInt(1);

                        if (pintries >= 3) {
                            throw new TooManyPinFails("User: " + target + " Too many failed pintries");
                        }
                        try {
                            dbcon.executeQuery(SQLStatements.CHECKPIN,
                                    new String[]{target, oldpin}, new OldPinFail("Oldpin wrong. user/oldpinhash: " + target + "/" + oldpin));
                        } catch (Exception e) {                                                         //check pin
                            dbcon.executeUpdate(SQLStatements.UPDATEPINTRIES, new String[]{String.valueOf(++pintries), target});                   //Increment tries
                            throw e;
                        }
                    }
                    out.write(nul);
                    out.flush();
                    dbcon.executeUpdate(SQLStatements.UPDATEPINTRIES, new String[]{"0", target});
                    String newpin = Hasher.hash(decrypt(Base64.getDecoder().decode(readToNull())));
                    dbcon.executeUpdate(SQLStatements.SETPIN, new String[]{newpin, user});                                                                   //user-send Part 2
                    logger.logFormatted("Changed Pin: " + target, client.toString());
                    break;
                case 4:
                    String date = decrypt(Base64.getDecoder().decode(readToNull()));
                    boolean all = in.read() == nul;
                    StringBuilder tmpResult = new StringBuilder();
                    getGroupAttendence(target, date, tmpResult, all);
                    String result = tmpResult.toString();
                    out.write(nul);
                    out.flush();
                    sendResult(result);
                    logger.logFormatted("Send Result: " + target, client.toString());
                    break;
                default:
                    throw new UnknownMode("Unknown Mode " + c);
            }
        } else {
            throw new UserNotPermissoned("User "+ user + " not permissioned");
        }
    }

    private void reader(String pw) throws WorkerException, SQLException, IOException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        dbcon.executeQuery(SQLStatements.CHECKREADER,
                new String[]{pw}, new ReaderNotFound("No Reader in database with keyhash: " + pw));

        out.write(nul);                                                                        //Header-answer
        out.flush();
        logger.logFormatted("Reader authenticated", client.toString());

        ResultSet tmp;
        String chip = decrypt(Base64.getDecoder().decode(readToNull()));
        tmp = dbcon.executeQuery(SQLStatements.GETUSER,
                new String[]{chip}, new ChipNotFound("Chipnumber: " + chip + " has no UserID in database"));                                                   //getUser

        String uid = tmp.getString(1);                                                          //Reader-send
        String rid = decrypt(Base64.getDecoder().decode(readToNull()));
        int choice = in.read();

        switch(choice) {

            case 0:                                                                               // Timestamp
                dbcon.executeQuery(SQLStatements.GETTIMELOGGABLE, new String[]{uid}, new UserNotTimelogable("userID: " + uid + " not allowed to timelog"));//Check if allowed to server.log

                tmp = dbcon.executeQuery(SQLStatements.GETORT, new String[]{rid, pw},
                        new ReaderPlaceNotFound("Cannot find place to readerID/keyhash combination: " + rid + "/" + pw));

                String ort = tmp.getString(1);
                dbcon.executeUpdate(SQLStatements.INSERTTIME,
                        new String[]{uid, ort, new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date())}); //Insert server.log to server.db
                out.write(nul);
                out.flush();

                logger.logFormatted("UserID: " + uid + " Wrote Timestamp to database", client.toString());
                break;

            case 1:                                                                                       //Security authentification
                int pintries;
                tmp = dbcon.executeQuery(SQLStatements.GETPINTRIES,
                        new String[]{uid}, new UserNotFoundReader("userID: " + uid + " not found"));
                pintries = tmp.getInt(1);
                tmp = dbcon.executeQuery(SQLStatements.GETLAGERBERECHTIGT, new String[]{uid}, new UserNotFoundReader("userID: " + uid + " not found"));
                if (tmp.getInt(1) == 0 || pintries >= 3) {                                 //Check if allowed to enter
                    throw new UserNotAllowed("userID: " + uid + " not allowed to enter storage");
                }

                out.write(nul);
                out.flush();
                logger.logFormatted("UserID: " + uid + " allowed to enter storage", client.toString());

                String pin = Hasher.hash(decrypt(Base64.getDecoder().decode(readToNull())));
                tmp = dbcon.executeQuery(SQLStatements.CHECKPIN,
                        new String[]{uid, pin}, new WrongPin("Wrong Pin " + uid +"/" + pin));
                if (Objects.requireNonNull(tmp).isClosed()) {                                                   //check pin
                    dbcon.executeUpdate(SQLStatements.UPDATEPINTRIES,
                            new String[]{String.valueOf(++pintries), uid});                   //Increment tries
                }
                dbcon.executeUpdate(SQLStatements.UPDATEPINTRIES, new String[]{"0", uid});      //delete tries
                logger.logFormatted("UserID : " + uid + " Door opens", client.toString());

                dbcon.executeUpdate(SQLStatements.INSERTLAGER,
                        new String[]{uid, new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date())});
                out.write(nul);                                                                         //allowed to enter
                out.flush();
                break;

            default:
                throw new UnknownMode("Unknown Mode " + choice);
        }
    }

    private void getGroupAttendence(String target, String date, StringBuilder result, boolean recursion) throws WorkerException, SQLException {
        ResultSet tmp = dbcon.executeQuery(SQLStatements.GETGROUPATTENDANCE
                , new String[]{date, target}, null);
        result.append(setToString(tmp,4));
        if(recursion) {
            tmp = dbcon.executeQuery(SQLStatements.GETCHILDGROUPS
                    , new String[]{target}, null);
            while(tmp.next()) {
                getGroupAttendence(tmp.getString(0),date,result,true);//recursion
            }
        }
    }

    private boolean isHigherAuthentificated(String user, String target, int choice) throws WorkerException, SQLException {
        ResultSet tmp = dbcon.executeQuery(SQLStatements.GETGROUP, new String[]{user}, new UnknownGroup("Couldn't get group to user: " + user));  //check group rights
        String groupasker = tmp.getString(1);

        String currentGroup;
        if(choice == 4) {
            currentGroup = target;
        } else {
            tmp = dbcon.executeQuery(SQLStatements.GETGROUP, new String[]{target}, new UnknownGroup("Couldn't get group to user: " + target));
            currentGroup = tmp.getString(1);
        }

        if(groupasker.equals(currentGroup)) {
            return false;
        }
        return isHigherAuthentificated(user, groupasker,currentGroup);
    }

    private boolean isHigherAuthentificated(String user, String groupasker, String currentGroup) throws SQLException, WorkerException {
            if(groupasker.equals(currentGroup)) {
                return true;
            }
            ResultSet tmp = dbcon.executeQuery(SQLStatements.GETSUPERGROUPS,
                    new String[]{currentGroup}, new UserNotPermissoned("User: " + user + " not permissioned."));
            while(tmp.next()) {
                currentGroup = tmp.getString(1);
                if(isHigherAuthentificated(user, groupasker, currentGroup)) {
                    return true;
                }
            }
            return false;
    }

    private String readToNull() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        byte x;
        while ((x = (byte) in.read()) > nul) {
            bytes.write(x);
        }
        return new String(bytes.toByteArray());
    }

    private String decrypt(byte[] message) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException,
            BadPaddingException, IllegalBlockSizeException {
        Cipher cip = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding");
        cip.init(Cipher.DECRYPT_MODE,kp.getPrivate());
        return new String(cip.doFinal(message),StandardCharsets.UTF_8);
    }

    private byte[] encrypt(String message) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException,
            BadPaddingException, IllegalBlockSizeException {
        Cipher cip = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding");
        cip.init(Cipher.ENCRYPT_MODE,pkclient);
        return cip.doFinal(message.getBytes(StandardCharsets.UTF_8));
    }

    private String setToString(ResultSet rs, int numFields) throws SQLException {
        StringBuilder result = new StringBuilder();
        while(rs.next()){
            for (int i = 1; i<=numFields ;i++) {
                result.append(rs.getString(i));
                result.append((i==numFields)?"\n":"|");
            }
        }
        return result.toString();
    }

    @SuppressWarnings("FieldCanBeLocal")
    private final int paddingBytes = 41;
    private void sendResult(String result) throws NoSuchAlgorithmException, InvalidKeySpecException,
            IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchPaddingException, IOException {
        KeyFactory kf = KeyFactory.getInstance("RSA");
        RSAPublicKeySpec spec = kf.getKeySpec(pkclient,RSAPublicKeySpec.class);
        int byteLength = spec.getModulus().bitLength()/8-paddingBytes;
        int bytes = 0;

        StringBuilder sb = new StringBuilder();

        for(int i=0;i<result.length();i++) {
            sb.append(result.charAt(i));
            bytes++;

            if (bytes >= byteLength - 3) { //Why 3?
		        out.write((byte) 1);
                out.write(Base64.getEncoder().encode(encrypt(sb.toString())));
                out.write(nul);
                bytes = 0;
                sb.setLength(0);
            }
        }
	    out.write(nul);
        out.write(Base64.getEncoder().encode(encrypt(sb.toString())));
        out.write(nul);
    }

    private void cleanup() {
        try {
            con.shutdownInput();
            con.shutdownOutput();
            logger.logFormatted("Thread shutting down",client.toString());
            out.close();
            in.close();
            con.close();
        } catch (Exception e) {
            logger.logFormatted(e.getMessage(),client.toString());
        }
    }
}
