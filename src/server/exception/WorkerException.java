package server.exception;

public abstract class WorkerException extends Throwable {
    private byte num;

    WorkerException(String message, byte num) {
        super(message);
        this.num = num;
    }

    public byte getNum() {
        return num;
    }
}






