package server.exception;

public class ReaderPlaceNotFound extends ReaderException {
    public ReaderPlaceNotFound(String message) {
        super(message, (byte) 5);
    }
}
