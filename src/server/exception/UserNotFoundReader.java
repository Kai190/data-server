package server.exception;

public class UserNotFoundReader extends ReaderException {
    public UserNotFoundReader(String message) {
        super(message, (byte) 6);
    }
}
