package server.exception;

public class UnknownMode extends WorkerException{
    public UnknownMode(String message) {
        super(message, (byte) 9);
    }
}
