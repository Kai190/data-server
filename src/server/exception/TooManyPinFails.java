package server.exception;

public class TooManyPinFails extends UserException {
    public TooManyPinFails(String message) {
        super(message, (byte) 4);
    }
}
