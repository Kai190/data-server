package server.exception;

public class TooManyPWFails extends UserException {
    public TooManyPWFails(String message) {
        super(message, (byte) 3);
    }
}
