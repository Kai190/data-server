package server.exception;

abstract class ReaderException extends WorkerException {
    ReaderException(String message, byte num) {
        super(message, num);
    }
}
