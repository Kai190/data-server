package server.exception;

public class ReaderNotFound extends ReaderException {
    public ReaderNotFound(String message) {
        super(message, (byte) 2);
    }
}
