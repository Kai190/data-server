package server.exception;

public class LoginFailed extends UserException {
    public LoginFailed(String message) {
        super(message, (byte) 5);
    }
}
