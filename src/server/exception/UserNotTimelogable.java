package server.exception;

public class UserNotTimelogable extends ReaderException {
    public UserNotTimelogable(String message) {
        super(message, (byte) 4);
    }
}
