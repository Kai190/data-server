package server.exception;

public class UserNotAllowed extends ReaderException {
    public UserNotAllowed(String message) {
        super(message, (byte) 7);
    }
}
