package server.exception;

public class UserNotPermissoned extends UserException {
    public UserNotPermissoned(String message) {
        super(message, (byte) 8);
    }
}
