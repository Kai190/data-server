package server.exception;

public class WrongPin extends ReaderException {
    public WrongPin(String message) {
        super(message, (byte) 8);
    }
}
