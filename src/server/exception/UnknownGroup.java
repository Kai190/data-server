package server.exception;

public class UnknownGroup extends UserException {
    public UnknownGroup(String message) {
        super(message, (byte) 7);
    }
}
