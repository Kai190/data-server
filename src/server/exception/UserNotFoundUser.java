package server.exception;

public class UserNotFoundUser extends UserException {
    public UserNotFoundUser(String message) {
        super(message, (byte) 2);
    }
}
