package server.exception;

public class ChipNotFound extends ReaderException {
    public ChipNotFound(String message) {
        super(message, (byte) 3);
    }
}
