package server.exception;

abstract class UserException extends WorkerException {
    UserException(String message, byte num) {
        super(message, num);
    }
}
