package server.exception;

public class OldPinFail extends UserException {
    public OldPinFail(String message) {
        super(message, (byte) 6);
    }
}
