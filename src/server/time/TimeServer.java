package server.time;

import server.log.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TimeServer extends Thread{
    private Logger logger;
    private ServerSocket serverSocket;
    private final int port = 6459;

    public TimeServer(Logger l) throws IOException {
        logger = l;
        serverSocket = new ServerSocket(port);                                                  //init Socket
    }

    @Override
    public void run() {
        //noinspection InfiniteLoopStatement
        while(true) {
            try {
                Socket con = serverSocket.accept();                                                //wait for connection
                logger.logFormatted("TIME: verbunden", con.getInetAddress().toString());
                TimeWorker tw = new TimeWorker(con, logger);
                tw.setDaemon(true);
                tw.start();                                                                         //start new server.time.TimeWorker Thread
            } catch (IOException e) {
                logger.logFormatted("Problem while connecting to Client", "server.time.TimeServer");
            }
        }
    }
}
