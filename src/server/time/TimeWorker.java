package server.time;

import server.log.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;

public class TimeWorker extends Thread{

    private Socket con;
    private Logger logger;
    private InetAddress client;
    private OutputStream out;
    private ByteBuffer bytes;

    TimeWorker(Socket c, Logger l) {
        con = c;                                                                              //init Socket and logger from server.time.TimeServer
        logger = l;
        client = con.getInetAddress();
        bytes = ByteBuffer.allocate(Integer.BYTES);                                            //int for 32 bit Systems
        try {
            out = con.getOutputStream();                                                       //init out Stream
        } catch (IOException e) {
            logger.logFormatted("TIME: Streams not initialized. Connection closed", client.toString());
            cleanup();
        }
    }
    public void run() {
        try {
            out.write((bytes.putInt((int)(System.currentTimeMillis()/1000))).array());      //int for 32 bit Systems
            logger.logFormatted("TIME: Time given", client.toString());
        } catch (IOException e) {
            logger.logFormatted("TIME: In- Out- or ConnectionError: " + e.getMessage(), client.toString());
        }
        cleanup();
    }

    private void cleanup() {                                                                //clean shutdown of Thread
        try {
            con.shutdownInput();
            con.shutdownOutput();
            logger.logFormatted("TIME: Thread shutting down",client.toString());
            out.close();
            con.close();
        } catch (Exception e) {
            logger.logFormatted(e.getMessage(),client.toString());
        }
    }
}
