package server.log;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger extends BufferedWriter {

    public Logger(Writer writer) {
        super(writer);
    }

    public void logFormatted(String text, String client){                          //server.log everything that happens
        try {
            write("Log at: "+client+" "+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+" "+text+"\n");
            flush();
            System.out.println("Log at: "+client+" "+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+" "+text);
        } catch (IOException e) {
            System.exit(1);
        }
    }

}
