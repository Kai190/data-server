package server;

import server.db.DBConnector;
import server.log.Logger;
import server.time.TimeServer;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

public class Server {

    private ServerSocket serverSocket;
    private KeyPair kp;
    private Logger logger;

    public Server(String path) {
        try {                                                                                           //init logger
            File log = new File("timeserver.log");
            //noinspection ResultOfMethodCallIgnored
            log.createNewFile();
            logger = new Logger(new FileWriter(log,true));
        } catch (IOException e) {
            System.exit(1);
        }
        logger.logFormatted("Logfile opened", "Server");
        try {
            TimeServer ts = new TimeServer(logger);
            ts.setDaemon(true);         //init Timeserver
            ts.start();
            logger.logFormatted("Timeserver started", "Server");
        } catch (IOException e) {
            logger.logFormatted("server.time.TimeServer not started", "Server");
        }

        try {
            int port = 6458;
            serverSocket = new ServerSocket(port);                                                   //init Socket and Keys
            KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA");
            int keysize = 4096;
            gen.initialize(keysize);
            kp = gen.genKeyPair();
            logger.logFormatted("Socket and Keys generated", "Server");
        } catch (IOException e) {
            logger.logFormatted("Socket not generated","Server");
            System.exit(1);
        } catch (NoSuchAlgorithmException e) {
            logger.logFormatted("Key not generated", "Server");
            System.exit(1);
        }
        try {                                                                                           //init database
            DBConnector dbcon = new DBConnector();
            dbcon.connect(path);
            Worker.setDbcon(dbcon);
            logger.logFormatted("Initialized Database Connection", "Server");
        } catch (SQLException e) {
            e.printStackTrace();
            logger.logFormatted("Couldn't initialize SQL Statement or connect to database","Server");
            System.exit(1);
        }
    }

    public void run() {
        logger.logFormatted("Server started", "Server");
        //noinspection InfiniteLoopStatement
        while (true) {
            try {
                Socket con = serverSocket.accept();                                                       //wait for connection
                logger.logFormatted("connected", con.getInetAddress().toString());
                Worker cp = new Worker(con, kp, logger);                                                  //create new WorkerThread
                cp.setDaemon(true);
                cp.start();
            } catch (IOException e) {
                logger.logFormatted("Problem while connecting to Client", "server.Server");
            }
        }
    }
}
