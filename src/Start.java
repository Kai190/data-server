import server.Server;

public class Start {
    public static void main(String[] args) {
        if(args.length>0) {
            Server s = new Server(args[0]);
            s.run();
        } else {
            System.out.println("path argument missing");
        }
    }
}
