package hash;

import java.math.BigInteger;
import java.security.*;
import java.util.Scanner;

public class Hasher {

    public static String hash(String message) throws NoSuchAlgorithmException {                //Hash message and put back to String
	MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(message.getBytes());
        byte[] bytes = md.digest();
        BigInteger no = new BigInteger(1, bytes);

        return no.toString(16);
    }

    public static void main(String[] args) {                                            //method if needed to call directly
        Scanner tastatur = new Scanner(System.in);
        System.out.println("Insert Text you want to hash: ");
        String message = tastatur.nextLine();
        try {
            message = hash(message);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        System.out.println("Result: " + message);
    }
}
